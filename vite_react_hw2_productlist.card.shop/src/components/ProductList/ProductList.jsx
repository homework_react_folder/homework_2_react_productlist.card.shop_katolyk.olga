import React from "react";
import PropTypes from "prop-types";
import ProductCard from "../ProductCard/ProductCard.jsx";
import "./ProductList.scss";

const ProductList = ({ products, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent }) => {
    return (
        <div className="product-list">
            {products.map(product => (
                <ProductCard 
                    key={product.article} 
                    product={product} 
                    handleImgZoomModal={handleImgZoomModal} 
                    handleFavoritedList={handleFavoritedList}
                    handleAddBasketModal={handleAddBasketModal} 
                    setModalContent={setModalContent}
                />
            ))}
        </div>
    );
};

ProductList.propTypes = {
    products: PropTypes.array,
    handleImgZoomModal: PropTypes.func,
    handleFavoritedList: PropTypes.func,
    handleAddBasketModal: PropTypes.func,
    setModalContent: PropTypes.func
};

export default ProductList; 
