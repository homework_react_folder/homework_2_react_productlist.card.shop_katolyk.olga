import React from "react";
import PropTypes from "prop-types";
import "./ProductCard.scss"
import Button from "../Button/Button.jsx";
import Zoom from "../../assets/icons/zoom.svg?react"
import Star from "../../assets/icons/star.svg?react"
import Basket from "../../assets/icons/basket.svg?react";

const ProductCard = ({ product, handleImgZoomModal, handleFavoritedList, handleAddBasketModal, setModalContent }) => {
    const { name, price, image, article, color, isFavorited } = product;

    return (
        <div className="product-card">
            <div className="product-card_icons">  
                <Button onClick={() => {
                    handleImgZoomModal();
                    setModalContent({imgSrc: image, nameCard: name,})
                    }}>< Zoom />
                </Button>              
                <Button 
                    className={isFavorited === true ? "isFavorited" : ""}
                    onClick={() => { 
                        handleFavoritedList(product);
                    }}>
                    < Star 
                    /> 
                </Button>
                <Button onClick={() => {
                    handleAddBasketModal();
                    setModalContent({
                        imgSrc: image, 
                        modalText: "Add to basket",
                        nameCard: name,
                        price: price,
                        article: article,
                        color: color
                    })
                    }} >< Basket />
                </Button>
            </div>
            <img src={image} alt={name} className="product-card_img" />
            <h2 className="product-card_title">{name}</h2>
            <p><span className="product-card_description">Price: </span>{price}$</p>
            <p><span className="product-card_description">Article: </span>{article}</p>
            <p><span className="product-card_description">Color: </span>{color}</p>
        </div>
    );
};

ProductCard.propTypes = {
    product: PropTypes.object,
    handleImgZoomModal: PropTypes.func,
    handleFavoritedList: PropTypes.func,
    handleAddBasketModal: PropTypes.func,
    setModalContent: PropTypes.func,
    name: PropTypes.string,
    price: PropTypes.number,
    image: PropTypes.any,
    article: PropTypes.string,
    color: PropTypes.string,
};

export default ProductCard;
