import React from "react";
import PropTypes from "prop-types";
import "../ModalBase/ModalBase.scss";
import "./ModalAddBasket.scss";
import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalHeader from "../ModalBase/ModalHeader.jsx"
import ModalBody from "../ModalBase/ModalBody.jsx";
import ModalFooter from "../ModalBase/ModalFooter.jsx";
import Basket from "../../assets/icons/basket.svg?react";
// import Button from "../Button/Button.jsx";

const ModalAddBasket = ({close, handleCountBasket, modalText, imgSrc, nameCard, price, article, color }) => {
    
    return (
        <ModalWrapper onClick={close}>
            <ModalBox>
                <ModalClose onClick={close}/>
                <img src={imgSrc} className="modal-header__img"/>
                <ModalHeader>{nameCard}</ModalHeader>
                <ModalBody>
                    Good choice: {color} toy,<br/>
                    article: {article}, price: {price}$
                </ModalBody>
                <ModalFooter firstText={modalText} firstClick={() => {
                    close(), 
                    handleCountBasket({imgSrc, nameCard, price, article, color});}}>
                    < Basket />
                </ModalFooter>
            </ModalBox>
        </ModalWrapper>
    )
};

ModalAddBasket.propTypes = {
    close: PropTypes.func,
    handleCountBasket: PropTypes.func,
    modalText: PropTypes.string,
    imgSrc: PropTypes.any,
    nameCard: PropTypes.string,
    price: PropTypes.number,
    article: PropTypes.string,
    color: PropTypes.string
};

export default ModalAddBasket;
