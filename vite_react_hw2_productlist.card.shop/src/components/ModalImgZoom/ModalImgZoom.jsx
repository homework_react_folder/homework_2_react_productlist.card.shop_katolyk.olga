import React from "react";
import PropTypes from "prop-types";
import "../ModalBase/ModalBase.scss";
import "./ModalImgZoom.scss";
import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalHeader from "../ModalBase/ModalHeader.jsx";

const ModalImgZoom = ({close, imgSrc, nameCard  }) => {
    return (
        <ModalWrapper onClick={close}>
            <ModalBox>
                <ModalClose onClick={close}/>
                <img src={imgSrc} className="modal-header__img-zoom"/>
                <ModalHeader>{nameCard}</ModalHeader>
            </ModalBox>
        </ModalWrapper>
    )
};

ModalImgZoom.propTypes = {
    close: PropTypes.func,
    imgSrc: PropTypes.any,
    nameCard: PropTypes.string
};

export default ModalImgZoom;