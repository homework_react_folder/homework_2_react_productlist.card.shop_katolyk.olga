import React, { useState, useEffect } from 'react';
import Button from './components/Button/Button.jsx';
import ModalAddBasket from './components/ModalAddBasket/ModalAddBasket.jsx';
import ProductList from './components/ProductList/ProductList.jsx';
import Star from "./assets/icons/star.svg?react"
import Basket from "./assets/icons/basket.svg?react";
import Logo from "./components/Logo/logo.svg?react";
import ModalImgZoom from './components/ModalImgZoom/ModalImgZoom.jsx';

const App = () => {
  const [products, setProducts] = useState([]);
  const [isImgZoomModal, setIsImgZoomModal] = useState(false);
  const [isAddBasketModal, setIsAddBasketModal] = useState(false);
  const [modalContent, setModalContent] = useState({
    imgSrc: undefined,
    modalText: undefined,
    nameCard: undefined,
    price: undefined,
    article: undefined,
    color: undefined
  });
  const [favoritedList, setFavoritedList] = useState([]);
  const [countBasket, setCountBasket] = useState([]);

  useEffect(() => {
    fetch("../public/products.json")
      .then(response => response.json())
      .then(data => { setProducts(data) })
      .catch(error => console.error("Error fetching products: ", error));
  }, []);

  const handleImgZoomModal = () => {
    setIsImgZoomModal((prevState) => !prevState);
  }

  const handleAddBasketModal = () => {
    setIsAddBasketModal((prevState) => !prevState);
  }

  const updateFavoriteCountInLocalStorage = (favoriteList) => {
    localStorage.setItem('favoritedList', favoriteList);
  }; // оновлення localStorage при зміні кількості товарів у вибраному

  const updateBasketCountInLocalStorage = (count) => {
    localStorage.setItem('countBasket', count.toString());
  }; // оновлення localStorage при зміні кількості товарів у кошику

  // Функція по кліку на зірочку 1).додає/видаляє товар в масиві обраних, 2).в масиві локал сторідж, 3).фарбує зірочку на картці
  const handleFavoritedList = (item) => {
    setProducts(prevProducts => {
      // Оновлюємо масив всіх товарів, корегуя в клікнутому товарі значення isFavorited на true/false
      const updatedProducts = prevProducts.map(product => {
        if (product.article === item.article) {
          // Переключаємо стан isFavorited true/false
          const isFavorited = !product.isFavorited;
          // Оновлюємо товар зі зміненим значенням isFavorited
          return { ...product, isFavorited };
        }
        return product;
      });

      // Оновлюємо список обраних товарів
      const updatedFavoritedList = updatedProducts.filter(product => product.isFavorited);
      setFavoritedList(updatedFavoritedList);

      // Оновлюємо localStorage з урахуванням зміни у списку обраних товарів
      updateFavoriteCountInLocalStorage(JSON.stringify(updatedFavoritedList));

      return updatedProducts;
    });
    // console.log('Forming list of favorites:', item); // товар, який додається/видаляється в обраному списку
    // console.log("FavoritedList:", favoritedList); // список обраних товарів з запізненням! на одну позицію, причина - синхронність консоля
    // console.log("products:", products); // список всіх товарів з запізненням! змін на одну позицію, причина - синхронність консоля
  }

  // useEffect(() => {
  //   console.log("FavoritedList with useEffect:", favoritedList);
  // }, [favoritedList]); // асинхронний список обраних товарів - він актуальний на момент кліка

  // useEffect(() => {
  //   console.log("products with useEffect:", products);
  // }, [products]); // асинхронний список всіх товарів - він актуальний на момент кліка з урахуванням усіх змін

  const handleCountBasket = (item) => {
    setCountBasket((prevState) => {
      const newBasketList = [...prevState, item];
      updateBasketCountInLocalStorage(JSON.stringify(newBasketList));
      return newBasketList;
    });

  }

  // useEffect(() => {
  //   console.log("countBasket with useEffect", countBasket)
  // }, [countBasket]); // асинхронний список товарів у кошику - він актуальний на момент кліка з урахуванням усіх змін

  return (
    <>
      <header className="header">
        <Button>< Logo /></Button>
        <h1 className="header_title">My toy-shop</h1>
        <div className="header_icons">
          <div className="header_star">
            <Button>< Star /></Button>
            <p className="header-icons_count">{favoritedList.length}</p>
          </div>
          <div className="header_basket">
            <Button>< Basket /></Button>
            <p className="header-icons_count">{countBasket.length}</p>
          </div>
        </div>
      </header>

      <main>
        <ProductList
          products={products}
          handleImgZoomModal={handleImgZoomModal}
          handleFavoritedList={handleFavoritedList}
          handleAddBasketModal={handleAddBasketModal}
          setModalContent={setModalContent}
        />
      </main>

      {isImgZoomModal && <ModalImgZoom
        close={handleImgZoomModal}
        imgSrc={modalContent.imgSrc}
        nameCard={modalContent.nameCard}
      />}

      {isAddBasketModal && <ModalAddBasket
        close={handleAddBasketModal}
        handleCountBasket={handleCountBasket}
        imgSrc={modalContent.imgSrc}
        modalText={modalContent.modalText}
        nameCard={modalContent.nameCard}
        color={modalContent.color}
        article={modalContent.article}
        price={modalContent.price}
      />}
    </>
  )
};

export default App;














// // Також, при завантаженні компоненту App, ми можемо перевірити наявність даних у localStorage і встановити початкові значення:
// // javascript
// 
// useEffect(() => {
//   const favoriteCount = localStorage.getItem('favoritedList');
//   const basketCount = localStorage.getItem('countBasket');

//   if (favoriteCount) {
//     setFavoritedList(parseInt(favoriteCount, 10));
//   }

//   if (basketCount) {
//     setCountBasket(parseInt(basketCount, 10));
//   }
// }, []);





// const handleFavoritedList = (item) => {
//   setFavoritedList((prevState) => {
//     // Перевіряємо, чи товар вже є у списку фаворитів
//     if (!prevState.some(product => product.article === item.article)) {
//       // Додаємо новий товар до списку фаворитів
//       const newFavoriteList = [...prevState, item];
//       // Оновлюємо localStorage
//       updateFavoriteCountInLocalStorage(JSON.stringify(newFavoriteList));
//       return newFavoriteList;
//     } else {
//       // Видаляємо товар зі списку фаворитів
//       const newFavoriteList = prevState.filter(product => product.article !== item.article);
//       // Оновлюємо localStorage
//       updateFavoriteCountInLocalStorage(JSON.stringify(newFavoriteList));
//       return newFavoriteList;
//     }
//   });
// };